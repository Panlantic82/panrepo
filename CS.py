import socket
import re

### TODO:
# logging module, Event (similar to Error, do not inherit)
# Daemonizer

class Error(Exception):
    def __init__(self, code, value):
        self.code = code
        self.value = value
    def __str__(self):
        return str(self.code + ': ' + self.value)

class Server():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = '127.0.0.1'
    port = 768
    address = (host, port)
    
    def __init__(self, host, port):
        self.sock.bind(address)
        sock.listen(10)
        self.run()

    def run(self):
        line_sep = re.compile(b'\r?\n')
        while True:
            try:
                conn, addr = sock.accept()
                data = conn.recv(1024)
                commands = line_sep.split(data)
                for command in commands:
                    if command != b'':
                        self.handle(command.decode('utf-8'))
            except:
                raise Error(100, 'Connection closed')
            
    def send(self, command):
        self.sock.send(str(command + '\r\n').encode('utf-8'))
        
    def handle(self, command):
        handlers = { 'HELLO' : self.hello }
        if command in handlers:
            handlers[command]()
        else:
            raise Error(101, 'Command not found')

    def hello(self):
        self.send('Zdarova')
        
class Client():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = '127.0.0.1'
    port = 768
    address = (host, port)
    
    def __init__(self, host, port):
        self.sock.connect(address)

    def run(self):
        line_sep = re.compile(b'\r?\n')
        while True:
            try:
                data = sock.recv(1024)
                commands = line_sep.split(data)
                for command in commands:
                    if command != b'':
                        self.handle(command.decode('utf-8'))
            except:
                raise Error(100, 'Connection closed')

    def send(self, command):
        self.sock.send(str(command + '\r\n').encode('utf-8'))
        
    def handle(self, command):
        handlers = { 'HELLO' : self.hello }
        if command in handlers:
            handlers[command]()
        else:
            raise Error(101, 'Command not found')

    def hello(self):
        self.send('Zdarova')



